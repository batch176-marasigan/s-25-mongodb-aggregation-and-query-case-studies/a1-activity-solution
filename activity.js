// Activity (aggregate -176)

//aggregate to count total number of items supplied by Red 
db.fruits.aggregate ([

	{$match:{supplier:"Red Farms Inc."}},    

	{$count:"itemsRedFarms"}     

])


//price > 50

db.fruits.aggregate ([
	{$match:{price:{$gt:50}}},     
	{$count:"priceGreaterThan50"}     
])


// average price
db.fruits.aggregate ([
	{$match:{onSale:true}},     
	{$group:{_id:"$supplier",   
	avgPricePerSupplier:{$avg: "$price"}}}  
])


// highest price of fruits onSale per supplier
db.fruits.aggregate ([
	{$match:{onSale:true}},     
	{$group:{_id:"$supplier",   
	maxPricePerSupplier:{$max: "$price"}}} 
])

  

// min price of fruits onSale per supplier
db.fruits.aggregate ([
	{$match:{onSale:true}},     
	{$group:{_id:"$supplier",   
	minPricePerSupplier:{$min: "$price"}}}  
])



